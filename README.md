
# SPELLBINDER


Dark, cold and low-contrast neovim theme, inspired by [Apprentice](https://github.com/romainl/Apprentice) theme.
Comes with treesitter bindings. Lualine coloring supported.


## Showcase

![Showcase](assets/screen.gif)

## Installing

### Using `lazy.nvim`

```lua
{ url = "https://codeberg.org/hatless/spellbinder.nvim" }
```

## Casting a spell

`:colorscheme spellbinder`

## Binding a spell

To enable this coloscheme by default:

### Inside `init.vim`

```vim
set background=dark
colorscheme spellbinder
```

### Inside `init.lua`

```lua
require("spellbinder").setup({
    -- Background color of the floating window
    -- Use "light" if you use floating window without borders, otherwise use "dark"
    float_window_background = "light"
})

local spellbinder = require("lualine.themes.spellbinder")
require("lualine").setup({
        options = {
        theme = spellbinder,
    },
})
```

If you don't need lualine, write something like this:

```lua
vim.o.background = "dark"
vim.cmd([[colorscheme spellbinder]])
```

## Color palette

| Intensity        | Normal                                                              | Intensity        | Bright                                                             |
|------------------|---------------------------------------------------------------------|------------------|--------------------------------------------------------------------|
| 0                | `#262626` ![#262626](https://placehold.it/15/262626/000000?text=.)  | 8                | `#444444` ![#444444](https://placehold.it/15/444444/000000?text=.) |
| 1                | `#AF5F5F` ![#AF5F5F](https://placehold.it/15/AF5F5F/000000?text=.)  | 9                | `#D78787` ![#D78787](https://placehold.it/15/D78787/000000?text=.) |
| 2                | `#5F875F` ![#5F875F](https://placehold.it/15/5F875F/000000?text=.)  | 10               | `#87AF87` ![#87AF87](https://placehold.it/15/87AF87/000000?text=.) |
| 3                | `#87875F` ![#87875F](https://placehold.it/15/87875F/000000?text=.)  | 11               | `#AFAF87` ![#AFAF87](https://placehold.it/15/AFAF87/000000?text=.) |
| 4                | `#5F87AF` ![#5F87AF](https://placehold.it/15/5F87AF/000000?text=.)  | 12               | `#87AFD7` ![#87AFD7](https://placehold.it/15/87AFD7/000000?text=.) |
| 5                | `#875F87` ![#875F87](https://placehold.it/15/875F87/000000?text=.)  | 13               | `#AF87AF` ![#AF87AF](https://placehold.it/15/AF87AF/000000?text=.) |
| 6                | `#5F8787` ![#5F8787](https://placehold.it/15/5F8787/000000?text=.)  | 14               | `#87AFAF` ![#87AFAF](https://placehold.it/15/87AFAF/000000?text=.) |
| 7                | `#6C6C6C` ![#6C6C6C](https://placehold.it/15/6C6C6C/000000?text=.)  | 15               | `#B2B2B2` ![#B2B2B2](https://placehold.it/15/B2B2B2/000000?text=.) |
| Foreground color | `#B2B2B2` ![#B2B2B2](https://placehold.it/15/B2B2B2/000000?text=.)  | Background color | `#262626` ![#262626](https://placehold.it/15/262626/000000?text=.) |

Here is a sample `~/.Xresources` for Linux/BSD users.

Import this into [terminal.sexy](http://terminal.sexy) to convert it to the appropriate color scheme format for your preferred terminal emulator:

    ! special
    *.foreground:   #b2b2b2
    *.background:   #262626
    *.cursorColor:  #b2b2b2

    ! black
    *.color0:       #262626
    *.color8:       #444444

    ! red
    *.color1:       #af5f5f
    *.color9:       #d78787

    ! green
    *.color2:       #5f875f
    *.color10:      #87af87

    ! yellow
    *.color3:       #87875f
    *.color11:      #afaf87

    ! blue
    *.color4:       #5f87af
    *.color12:      #87afd7

    ! magenta
    *.color5:       #875f87
    *.color13:      #af87af

    ! cyan
    *.color6:       #5f8787
    *.color14:      #87afaf

    ! white
    *.color7:       #6c6c6c
    *.color15:      #b2b2b2
