local colors = {
    red         = "#af5f5f",
    green       = "#5f875f",
    yellow      = "#87875f",
    blue        = "#5f87af",
    magenta     = "#875f87",
    cyan        = "#5f8787",
    fg          = "#b2b2b2",
    bg          = "#262626",
    gray        = "#303030",
    gray_bright = "#6c6c6c",
}

return {

    normal = {
        a = { fg = colors.bg, bg = colors.blue },
        b = { fg = colors.blue, bg = colors.gray },
        c = { fg = colors.fg, bg = colors.bg },
    },

    insert = {
        a = { fg = colors.bg, bg = colors.green },
        b = { fg = colors.green, bg = colors.gray },
    },

    visual = {
        a = { fg = colors.bg, bg = colors.magenta },
        b = { fg = colors.magenta, bg = colors.gray },
    },

    command = {
        a = { fg = colors.bg, bg = colors.cyan },
        b = { fg = colors.cyan, bg = colors.gray },
    },

    replace = {
        a = { fg = colors.bg, bg = colors.red },
        b = { fg = colors.red, bg = colors.gray },
    },

    inactive = {
        a = { fg = colors.blue, bg = colors.bg, },
        b = { fg = colors.gray_bright, bg = colors.bg, gui = "bold" },
        c = { fg = colors.gray_bright, bg = colors.bg, },
    },
}
