local M = {}
local c = require("spellbinder.palette")

function M.setup()
    vim.g.terminal_color_0 = c.bg
    vim.g.terminal_color_1 = c.red
    vim.g.terminal_color_2 = c.green
    vim.g.terminal_color_3 = c.yellow
    vim.g.terminal_color_4 = c.blue
    vim.g.terminal_color_5 = c.magenta
    vim.g.terminal_color_6 = c.cyan
    vim.g.terminal_color_7 = c.gray_bright
    vim.g.terminal_color_8 = c.gray
    vim.g.terminal_color_9 = c.red_bright
    vim.g.terminal_color_10 = c.green_bright
    vim.g.terminal_color_11 = c.yellow_bright
    vim.g.terminal_color_12 = c.blue_bright
    vim.g.terminal_color_13 = c.magenta_bright
    vim.g.terminal_color_14 = c.cyan_bright
    vim.g.terminal_color_15 = c.fg
end

return M
