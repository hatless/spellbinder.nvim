local M = {
    default_options = {
        float_window_background = 'gray_dark'
    }
}
local theme = require("spellbinder.theme")
local terminal = require("spellbinder.terminal")

M.setup = function(user_config)
    user_config = user_config or M.default_options

    vim.cmd("hi clear")

    vim.o.background = "dark"
    if vim.fn.exists("syntax_on") then
        vim.cmd("syntax reset")
    end

    vim.o.termguicolors = true
    vim.g.colors_name = "spellbinder"

    theme.set_highlights(user_config)
    terminal.setup()
end

return M
