local colors = {
    bg = "#262626",
    fg = "#b2b2b2",
    gray = "#444444",
    gray_bright = "#6c6c6c",
    gray_dark = "#303030",
    red = "#af5f5f",
    red_bright = "#d78787",
    green = "#5f875f",
    green_bright = "#87af87",
    yellow = "#87875f",
    yellow_bright = "#afaf87",
    blue = "#5f87af",
    blue_bright = "#87afd7",
    magenta = "#875f87",
    magenta_bright = "#af87af",
    cyan = "#5f8787",
    cyan_bright = "#87afaf",
}

return colors
